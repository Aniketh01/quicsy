#include "quicsy_tls.h"

static ngtcp2_crypto_level
quic_from_ossl_level(OSSL_ENCRYPTION_LEVEL ossl_level) {
	switch (ossl_level) {
		case ssl_encryption_initial:
			return NGTCP2_CRYPTO_LEVEL_INITIAL;
		case ssl_encryption_early_data:
			return NGTCP2_CRYPTO_LEVEL_EARLY;
		case ssl_encryption_handshake:
			return NGTCP2_CRYPTO_LEVEL_HANDSHAKE;
		case ssl_encryption_application:
			return NGTCP2_CRYPTO_LEVEL_APP;
		default:
			assert(0);
	}
}

static int setup_initial_crypto_context(struct quicsocket *qs) {
	const ngtcp2_cid *dcid = ngtcp2_conn_get_dcid(qs->qconn);

	if (ngtcp2_crypto_derive_and_install_initial_key(
				qs->qconn, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, dcid,
				NGTCP2_CRYPTO_SIDE_CLIENT) != 0)
		return -1;

	return 0;
}

static int quic_init_ssl(struct quicsocket *qs)
{
	const uint8_t *alpn = NULL;
	size_t alpnlen = 0;

	const char *hostname = qs->conn->hostname;

	if(qs->ssl)
		SSL_free(qs->ssl);

	qs->ssl = SSL_new(qs->sslctx);

	SSL_set_app_data(qs->ssl, qs);
	SSL_set_connect_state(qs->ssl);

	switch(qs->version) {
#ifdef NGTCP2_PROTO_VER
		case NGTCP2_PROTO_VER:
			alpn = (const uint8_t *)NGTCP2_ALPN_H3;
			alpnlen = sizeof(NGTCP2_ALPN_H3) - 1;
			break;
#endif
	}
	if(alpn)
		SSL_set_alpn_protos(qs->ssl, alpn, (int)alpnlen);

	/* set SNI */
	SSL_set_tlsext_host_name(qs->ssl, hostname);
	return 0;
}