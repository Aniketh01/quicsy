#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <vector>
#include <random>

using namespace std;

struct Buffer {
	Buffer(const uint8_t *data, size_t datalen);
	explicit Buffer(size_t datalen);

	size_t size() const { return tail - begin; }
	size_t left() const { return buf.data() + buf.size() - tail; }
	uint8_t *const wpos() { return tail; }
	const uint8_t *rpos() const { return begin; }
	void push(size_t len) { tail += len; }
	void reset() { tail = begin; }

	std::vector<uint8_t> buf;
	// begin points to the beginning of the buffer.  This might point to
	// buf.data() if a buffer space is allocated by this object.  It is
	// also allowed to point to the external shared buffer.
	uint8_t *begin;
	// tail points to the position of the buffer where write should
	// occur.
	uint8_t *tail;
};



#endif
