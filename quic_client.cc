#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>

#include <ngtcp2/ngtcp2.h>
#include <nghttp3/nghttp3.h>
#include <ngtcp2/ngtcp2_crypto.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <ev.h>

#include "quicsy.h"
#include"utils.h"
#include "quicsy_tls.h"

#define QUIC_MAX_STREAMS (256 * 1024)
#define QUIC_MAX_DATA (1 * 1024 * 1024)
#define QUIC_IDLE_TIMEOUT 60000 /* milliseconds */
#define QUIC_CIPHERS                                                           \
	"TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_"                \
	"POLY1305_SHA256:TLS_AES_128_CCM_SHA256"
#define QUIC_GROUPS "P-256:X25519:P-384:P-521"


mt19937 make_mt19937() {
	random_device rd;
	return mt19937(rd());
}

auto randgen = make_mt19937();

static ngtcp2_tstamp timestamp(void) {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * NGTCP2_SECONDS + tv.tv_usec * NGTCP2_MICROSECONDS;
}

static FILE *keylog_file; /* not thread-safe */
static void keylog_callback(const SSL *ssl, const char *line) {
	(void)ssl;
	fputs(line, keylog_file);
	fputc('\n', keylog_file);
	fflush(keylog_file);
}

static int initial_cb(ngtcp2_conn *quic, void *user_data) {
	struct quicsocket *qs = (struct quicsocket *)user_data;

	if (ngtcp2_crypto_read_write_crypto_data(
				quic, qs->ssl, NGTCP2_CRYPTO_LEVEL_INITIAL, NULL, 0) != 0)
		return NGTCP2_ERR_CALLBACK_FAILURE;

	return 0;
}

static int cb_recv_crypto_data(ngtcp2_conn *tconn,
		ngtcp2_crypto_level crypto_level,
		uint64_t offset, const uint8_t *data,
		size_t datalen, void *user_data) {
	struct quicsocket *qs = (struct quicsocket *)user_data;
	(void)offset;

	if (ngtcp2_crypto_read_write_crypto_data(tconn, qs->ssl, crypto_level, data,
				datalen) != 0)
		return NGTCP2_ERR_CRYPTO;

	return 0;
}

int handshake_completed(quicsocket *qs) {
	if (qs->_early_data &&
			SSL_get_early_data_status(qs->ssl) != SSL_EARLY_DATA_ACCEPTED) {
		return QUICSY_EARLY_DATA_ERR;
	}

	debug_printf("Negotiated cipher suite: ", SSL_get_cipher_name(qs->ssl));

	const unsigned char *alpn = nullptr;
	unsigned int alpnlen;

	SSL_get0_alpn_selected(qs->ssl, &alpn, &alpnlen);
	if (alpn) {
		debug_printf("Negotiated ALPN: ", alpn);
	}
	return QUICSY_OK;
}

static int cb_handshake_completed(ngtcp2_conn *tconn, void *user_data) {
	struct quicsocket *qs = (struct quicsocket *)user_data;
	(void)tconn;

	if (handshake_completed(qs) != 0) {
		return NGTCP2_ERR_CALLBACK_FAILURE;
	}

	return QUICSY_OK;
}

int hp_mask_cb(ngtcp2_conn *conn, uint8_t *dest, const ngtcp2_crypto_cipher *hp,
		const uint8_t *hp_key, const uint8_t *sample, void *user_data) {
	if (ngtcp2_crypto_hp_mask(dest, hp, hp_key, sample) != 0) {
		return NGTCP2_ERR_CALLBACK_FAILURE;
	}

	return QUICSY_OK;
}

quic_err_t quic_rand(u_int8_t *buf, size_t len) {
	int fd;

	fd = open("/dev/urandom", O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		return QUICSY_SYSTEM_ERR;
	} // end if

	read(fd, buf, len);
	close(fd);
	return QUICSY_OK;
}

int rand_cb(ngtcp2_conn *conn, uint8_t *dest, size_t destlen,
		ngtcp2_rand_ctx ctx, void *user_data) {

	if (quic_rand(dest, destlen) == QUICSY_OK) {
		return 0;
	}

	return NGTCP2_ERR_CALLBACK_FAILURE;
}

int recv_stream_data_cb(ngtcp2_conn *conn, int64_t stream_id, int fin,
		uint64_t offset, const uint8_t *data, size_t datalen,
		void *user_data, void *stream_user_data) {

	ngtcp2_conn_extend_max_stream_offset(conn, stream_id, datalen);
	ngtcp2_conn_extend_max_offset(conn, datalen);
	return 0;
}

static void quic_settings(ngtcp2_settings *s, uint64_t stream_buffer_size) {
	ngtcp2_settings_default(s);

	s->log_printf = log_printf;
	s->initial_ts = timestamp();
	s->transport_params.initial_max_stream_data_bidi_local = stream_buffer_size;
	s->transport_params.initial_max_stream_data_bidi_remote = QUIC_MAX_STREAMS;
	s->transport_params.initial_max_stream_data_uni = QUIC_MAX_STREAMS;
	s->transport_params.initial_max_data = QUIC_MAX_DATA;
	s->transport_params.initial_max_streams_bidi = 1;
	s->transport_params.initial_max_streams_uni = 3;
	s->transport_params.idle_timeout = QUIC_IDLE_TIMEOUT;
}

size_t remove_tx_stream_data(std::deque<Buffer> &d, uint64_t &tx_offset,
		uint64_t offset) {
	size_t len = 0;
	for (; !d.empty() && tx_offset + d.front().size() <= offset;) {
		tx_offset += d.front().size();
		len += d.front().size();
		d.pop_front();
	}
	return len;
}

void remove_tx_crypto_data(ngtcp2_crypto_level crypto_level, const uint64_t offset,
		size_t datalen) {
	struct quicsocket *qs = (struct quicsocket *)offset;
	auto &crypto = qs->_crypto_data[crypto_level];

	remove_tx_stream_data(crypto.data, crypto.acked_offset, offset + datalen);
}

int acked_crypto_offset_cb(ngtcp2_conn * conn,
		ngtcp2_crypto_level crypto_level, uint64_t offset,
		size_t datalen, void *user_data) {
	remove_tx_crypto_data(crypto_level, offset, datalen);

	return QUICSY_OK;
}

/**
 * TODO: HTTP3 agnoistic implementation for QUIC related ngtcp2 callbacks.
 *
 * Need to verify if this is the only design option we have.
 */
int acked_stream_data_offset_cb(ngtcp2_conn *conn, int64_t stream_id,
		uint64_t offset, size_t datalen, void *user_data,
		void *stream_user_data) {
	int rv;
	struct quicsocket *qs = (struct quicsocket *)user_data;

	rv = nghttp3_conn_add_ack_offset(qs->h3conn, stream_id, datalen);
	if (rv != 0) {
		debug_printf("nghttp3_conn_add_ack_offset returned error: %s\n",
				nghttp3_strerror(rv));
		return NGTCP2_ERR_CALLBACK_FAILURE;
	}
}

/**
 * TODO: HTTP3 agnoistic implementation for QUIC related ngtcp2 callbacks.
 *
 * Need to verify if this is the only design option we have.
 */
static int stream_close_cb(ngtcp2_conn *tconn, int64_t stream_id,
		uint64_t app_error_code,
		void *user_data, void *stream_user_data) {
	struct quicsocket *qs = (struct quicsocket *)user_data;
	int rv;
	/* stream is closed... */

	rv = nghttp3_conn_close_stream(qs->h3conn, stream_id,
			app_error_code);
	if(rv != 0) {
		debug_printf("nghttp3_conn_close_stream returned error: %s\n",
				nghttp3_strerror(rv));
		return NGTCP2_ERR_CALLBACK_FAILURE;
	}

	return 0;
}

static int recv_retry_cb(ngtcp2_conn *tconn, const ngtcp2_pkt_hd *hd,
		const ngtcp2_pkt_retry *retry, void *user_data)
{
	/* Re-generate handshake secrets here because connection ID might change. */
	struct quicsocket *qs = (struct quicsocket *)user_data;

	setup_initial_crypto_context(qs);

	return 0;
}

/**
 * NOTE: Could be used to spawn many bidirectional streams.
 *
 * This can be useful while we try to stream media or during custom QUIC development.
 */
static int extend_max_local_streams_bidi_cb(ngtcp2_conn *tconn,
		uint64_t max_streams,
		void *user_data)
{
	return 0;
}

static int get_new_connection_id_cb(ngtcp2_conn *tconn, ngtcp2_cid *cid,
		uint8_t *token, size_t cidlen,
		void *user_data)
{
	auto dis = std::uniform_int_distribution<uint8_t>(0, 255);
	auto f = [&dis]() { return dis(randgen); };

	std::generate_n(cid->data, cidlen, f);
	cid->datalen = cidlen;
	std::generate_n(token, NGTCP2_STATELESS_RESET_TOKENLEN, f);

	return 0;
}

int remove_connection_id_cb(ngtcp2_conn *conn, const ngtcp2_cid *cid,
		void *user_data) {
	return 0;
}

/**
 * TODO: HTTP3 agnoistic implementation for QUIC related ngtcp2 callbacks.
 *
 * Need to verify if this is the only design option we have.
 */
static int stream_reset_cb(ngtcp2_conn *tconn, int64_t stream_id,
		uint64_t final_size, uint64_t app_error_code,
		void *user_data, void *stream_user_data)
{
	struct quicsocket *qs = (struct quicsocket *)user_data;
	int rv;

	rv = nghttp3_conn_reset_stream(qs->h3conn, stream_id);
	if(rv != 0) {
		debug_printf("nghttp3_conn_reset_stream returned error: %s\n",
				nghttp3_strerror(rv));
		return NGTCP2_ERR_CALLBACK_FAILURE;
	}

	return 0;
}

/**
 * TODO: HTTP3 agnoistic implementation for QUIC related ngtcp2 callbacks.
 *
 * Need to verify if this is the only design option we have.
 */
static int extend_max_stream_data_cb(ngtcp2_conn *tconn, int64_t stream_id,
		uint64_t max_data, void *user_data,
		void *stream_user_data)
{
	struct quicsocket *qs = (struct quicsocket *)user_data;
	int rv;

	rv = nghttp3_conn_unblock_stream(qs->h3conn, stream_id);
	if(rv != 0) {
		debug_printf("nghttp3_conn_unblock_stream returned error: %s\n",
				nghttp3_strerror(rv));
		return NGTCP2_ERR_CALLBACK_FAILURE;
	}

	return 0;
}

static ngtcp2_conn_callbacks ngquic_callbacks = {
	initial_cb,
	nullptr, // recv_client_initial
	cb_recv_crypto_data,
	cb_handshake_completed,
	nullptr, // recv_version_negotiation
	ngtcp2_crypto_encrypt_cb,
	ngtcp2_crypto_decrypt_cb,
	hp_mask_cb,
	recv_stream_data_cb,
	acked_crypto_offset_cb, // REVIEW: related func needs proper review,
	acked_stream_data_offset_cb,
	nullptr, // stream_open
	stream_close_cb,
	nullptr, // recv_stateless_reset
	recv_retry_cb,
	extend_max_local_streams_bidi_cb,
	nullptr, // extend_max_streams_uni
	rand_cb,
	get_new_connection_id_cb,
	remove_connection_id_cb,
	nullptr,
	nullptr,
	nullptr,
	stream_reset_cb,
	nullptr, // extend_max_remote_streams_bidi,
	nullptr, // extend_max_remote_streams_uni,
	extend_max_stream_data_cb,
};