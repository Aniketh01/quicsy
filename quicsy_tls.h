#ifndef QUICSY_TLS
#define QUICSY_TLS


#include <assert.h>
#include <ngtcp2/ngtcp2.h>
#include <ngtcp2/ngtcp2_crypto.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

#include "debug.h"
#include "quicsy.h"

static ngtcp2_crypto_level
quic_from_ossl_level(OSSL_ENCRYPTION_LEVEL ossl_level);

static int setup_initial_crypto_context(struct quicsocket *qs);
#endif