#ifndef DEBUG_H
#define DEBUG_H

#include <stdarg.h>
#include <stdio.h>

void log_printf(void *user_data, const char *fmt, ...);
void printf_stream_data(int64_t stream_id, const uint8_t *data, size_t datalen);

#define DEBUG 1
#define debug_printf(fmt, ...)                                                  \
	do {                                                                         \
		fprintf(stderr, fmt, __VA_ARGS__);                                       \
	} while (0)

void log_printf(void *user_data, const char *fmt, ...) {
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr, "\n");
}

void printf_stream_data(int64_t stream_id, const uint8_t *data, size_t datalen) {
	fprintf(stderr, "Ordered STREAM data stream_id=0x%" PRIx64 "\n", stream_id);
}

#endif
